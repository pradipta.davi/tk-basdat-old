from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from .forms import *
from django.db import connection, transaction
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponse, HttpResponseRedirect


def landing(request):
    # context = {}
    if "isLogin" in request.session:
        return render(request, 'home.html')
    else:
        return render(request, 'start.html')

def home(request):
    response = {}
    if (request.session['role'] == 'Pemain'):
        username = request.session['username']
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.PEMAIN WHERE username='{username}'")
                pemain = cursor.fetchone()
                response = {'pemain': pemain[0], 'email': pemain[1], 'no_hp': pemain[3], 'koin': pemain[4]}
        except Exception as e:
                messages.info(request, str(e))
    return render(request, 'home.html', response)

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def login(request):
    if "isLogin" in request.session:
        return redirect('/')
    response={}
    response['error'] = False
    form = LoginForm(request.POST or None)
    response['form'] = form
    if(request.method == 'POST'):
        if(form.is_valid()) :
            username = request.POST['username']
            password = request.POST['password']
            try:
                with connection.cursor() as cursor:
                    cursor.execute(f"SELECT * FROM THE_CIMS.ADMIN as i, THE_CIMS.AKUN as p WHERE i.username = p.username AND i.username='{username}' AND i.password = '{password}'")
                    admin = cursor.fetchone()
                    cursor.execute(f"SELECT * FROM THE_CIMS.PEMAIN as o, THE_CIMS.AKUN as p WHERE o.username = p.username AND o.username='{username}' AND o.password = '{password}'")
                    pemain = cursor.fetchone()
                if admin:
                    request.session['username'] = admin[0]
                    request.session['role'] = "Admin"
                    request.session['isLogin'] = True
                    return HttpResponseRedirect('/home/')
                elif pemain:
                    request.session['username'] = pemain[0]
                    request.session['role'] = "Pemain"
                    request.session['isLogin'] = True
                    return HttpResponseRedirect('/home/')
                else:
                    messages.info(request, 'Username atau password anda salah!')
                    return render(request, 'login.html', response)
            except Exception as e:
                messages.info(request, str(e))
                return render(request, 'login.html', response)
        else:
            return render(request, 'login.html', response)
    else:
        form = LoginForm(request.POST or None)
        response['form'] = form
        return render(request,'login.html',response)

def logout(request):
    if "username" in request.session:
        request.session.flush()
        return redirect('/')
    return redirect('/')


def register(request):
    if "isLogin" in request.session:
        return redirect('/')
    return render(request, 'register.html')


@transaction.atomic
def register_admin(request):
    if "isLogin" in request.session:
        return redirect('/')
    response = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        try:
            with connection.cursor() as cursor:
                cursor.execute("""
                    INSERT INTO the_cims.akun
                    VALUES (%s)
                """, [username])
                cursor.execute("""
                    INSERT INTO the_cims.admin
                    VALUES (%s, %s)
                """, [username, password])
                request.session['username'] = username
                request.session['role'] = "Admin"
                request.session['isLogin'] = True
                return HttpResponseRedirect('/home/')
        except Exception as e:
            messages.info(request, str(e))


    response['error'] = False
    form = RegisterAdminForm(request.POST or None)
    response['form'] = form
    return render(request, 'register_admin.html', response)


@transaction.atomic
def register_pemain(request):
    if "isLogin" in request.session:
        return redirect('/')
    response = {}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        no_hp = request.POST['no_hp']
        try:
            with connection.cursor() as cursor:
                cursor.execute("""
                    INSERT INTO the_cims.akun
                    VALUES (%s)
                """, [username])
                cursor.execute("""
                    INSERT INTO the_cims.pemain
                    VALUES (%s, %s, %s, %s, %s)
                """, [username, email, password, no_hp, 0])
                request.session['username'] = username
                request.session['role'] = "Pemain"
                request.session['isLogin'] = True
                return HttpResponseRedirect('/home/')
        except Exception as e:
            messages.info(request, str(e))


    response['error'] = False
    form = RegisterPemainForm(request.POST or None)
    response['form'] = form
    return render(request, 'register_pemain.html', response)
