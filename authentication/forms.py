from django import forms

class LoginForm(forms.Form):
    username=forms.CharField(label='username',max_length=50)
    password=forms.CharField(label='Password',widget=forms.PasswordInput, max_length = 50)


class RegisterAdminForm(forms.Form):
    username = forms.CharField(label='username', max_length=50)
    password = forms.CharField(label='password', widget=forms.PasswordInput, max_length=20)


class RegisterPemainForm(forms.Form):
    username = forms.CharField(label='username', max_length=50)
    password = forms.CharField(label='password', widget=forms.PasswordInput, max_length=20)
    email = forms.CharField(label='email', max_length=50)
    no_hp = forms.CharField(label='no_hp', max_length=12)
