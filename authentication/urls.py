from django.urls import path
from . import views
app_name = 'authentication'
urlpatterns = [
    path('', views.landing, name='landing'),
    path('login/', views.login, name = 'login'),
    path('logout/', views.logout, name = 'logout'),
    path('home/', views.home, name = 'home'),
    path('register/', views.register, name='register'),
    path('register_admin/', views.register_admin, name='register_admin'),
    path('register_pemain/', views.register_pemain, name='register_pemain'),
]