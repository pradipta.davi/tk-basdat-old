from django.urls import path
from . import views
app_name = 'deleter'
urlpatterns = [
    path('hapus_warna_kulit/<str:kode>', views.hapus_warna_kulit, name='hapus_warna_kulit'),
    path('hapus_level/<int:level>', views.hapus_level, name='hapus_level'),
    path('hapus_menggunakan_apparel/<str:id_tokoh_pengguna>', views.hapus_menggunakan_apparel, name='hapus_menggunakan_apparel'),
    path('hapus_misi_utama/<str:nama_misi>', views.hapus_misi_utama, name='hapus_misi_utama'),
    path('hapus_makanan/<str:nama_makanan>', views.hapus_makanan, name='hapus_makanan'),
]
