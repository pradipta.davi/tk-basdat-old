from django.shortcuts import render, redirect
from django.db import connection, transaction
from django.http import HttpResponseRedirect
from authentication.views import login

# Create your views here.
def hapus_warna_kulit(request, kode):
    kode_warna = "#"+str(kode)
    
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        try :
            cursor = connection.cursor()
            cursor.execute("DELETE FROM the_cims.warna_kulit where kode ='"+kode_warna+"'")
        except Exception as e:
            print(str(e))
        return redirect('/read/list_warna_kulit')
    elif request.session['role'] == 'Pemain':
        return redirect('/')
    else:
        return login(request)
    return redirect('/')

def hapus_level(request, level):
    
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        try :
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM the_cims.level where level={level}")
        except Exception as e:
            print(str(e))
        return redirect('/read/list_level')
    elif request.session['role'] == 'Pemain':
        return redirect('/')
    else:
        return login(request)
    return redirect('/')

def hapus_menggunakan_apparel(request, id_tokoh_pengguna):
    
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        return redirect('/')
    elif request.session['role'] == 'Pemain':
        paramlist = id_tokoh_pengguna.split("-")
        id_koleksi = paramlist[0]
        tokoh = paramlist[1]
        pengguna = paramlist[2]
        try :
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM the_cims.menggunakan_apparel WHERE id_koleksi = '{id_koleksi}' AND nama_tokoh = '{tokoh}' AND username_pengguna = '{pengguna}' ")
        except Exception as e:
            print(str(e))
        return redirect('/read/list_menggunakan_apparel')
    else:
        return login(request)
    return redirect('/')


@transaction.atomic
def hapus_misi_utama(request, nama_misi):
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            cursor = connection.cursor()
            cursor.execute("DELETE FROM the_cims.misi_utama where nama_misi = %s", [nama_misi])
            cursor.execute("DELETE FROM the_cims.misi where nama = %s", [nama_misi])
            cursor.close()
        except Exception as e:
            print(str(e))
        return redirect('/read/list_misi_utama')
    elif request.session['role'] == 'Pemain':
        return redirect('/')
    else:
        return login(request)


def hapus_makanan(request, nama_makanan):
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            cursor = connection.cursor()
            cursor.execute("DELETE FROM the_cims.makanan where nama = %s", [nama_makanan])
            cursor.close()
        except Exception as e:
            print(str(e))
        return redirect('/read/list_makanan')
    elif request.session['role'] == 'Pemain':
        return redirect('/')
    else:
        return login(request)
