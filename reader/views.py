from django.shortcuts import render
from django.contrib import messages
from django.db import connection, transaction
from authentication.views import login
from django.shortcuts import redirect
from collections import namedtuple
from .forms import *

# Create your views here.

def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def list_tokoh(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.TOKOH")
                list_tokoh = cursor.fetchall()
                response = {'list_tokoh': list_tokoh}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.TOKOH WHERE username_pengguna='{request.session['username']}'")
                list_tokoh = cursor.fetchall()
                response = {'list_tokoh': list_tokoh}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_tokoh.html', response)

def list_menggunakan_barang(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT username_pengguna, nama_tokoh, nama, waktu FROM the_cims.menggunakan_barang m JOIN the_cims.koleksi_jual_beli k ON m.id_barang = k.id_koleksi JOIN the_cims.barang a ON k.id_koleksi = a.id_koleksi")
                list_menggunakan_barang = cursor.fetchall()
                response = {'list_menggunakan_barang': list_menggunakan_barang}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"")
                list_menggunakan_barang = cursor.fetchall()
                response = {'list_menggunakan_barang': list_menggunakan_barang}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_menggunakan_barang.html', response)

# T_T

def list_pekerjaan(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT nama,  base_honor, CASE
                                   WHEN nama NOT IN (SELECT nama_pekerjaan FROM THE_CIMS.APPAREL)
                                   AND nama NOT IN (SELECT nama_pekerjaan FROM THE_CIMS.BEKERJA)
                                   AND nama NOT IN (SELECT nama_pekerjaan FROM THE_CIMS.MISI_PEKERJAAN)
                                   AND nama NOT IN (SELECT nama_pekerjaan FROM THE_CIMS.PEKERJAAN_COCOK)   
                                   THEN 'true' ELSE 'false' END CAN_DELETE FROM THE_CIMS.PEKERJAAN""")
                list_pekerjaan = cursor.fetchall()
                response = {'list_pekerjaan': list_pekerjaan}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.PEKERJAAN")
                list_pekerjaan = cursor.fetchall()
                response = {'list_pekerjaan': list_pekerjaan}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_pekerjaan.html', response)

def list_bekerja(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT username_pengguna, nama_tokoh, timestamp, nama_pekerjaan, keberangkatan_ke, honor, CASE WHEN ")
                list_bekerja = cursor.fetchall()
                response = {'list_bekerja': list_bekerja}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.BEKERJA")
                list_bekerja = cursor.fetchall()
                response = {'list_bekerja': list_bekerja}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_bekerja.html', response)

def list_warna_kulit(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT KODE, CASE WHEN KODE NOT IN (SELECT WARNA_KULIT FROM THE_CIMS.TOKOH) THEN 'true' ELSE 'false' END CAN_DELETE FROM THE_CIMS.WARNA_KULIT")
                list_warna_kulit = cursor.fetchall()
                response = {'list_warna_kulit': list_warna_kulit}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.WARNA_KULIT")
                list_warna_kulit = cursor.fetchall()
                response = {'list_warna_kulit': list_warna_kulit}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_warna_kulit.html', response)


def list_level(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)
    
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT LEVEL, XP, CASE WHEN LEVEL NOT IN (SELECT LEVEL FROM THE_CIMS.TOKOH) THEN 'true' ELSE 'false' END CAN_DELETE FROM THE_CIMS.LEVEL ORDER BY LEVEL ASC")
                list_level = cursor.fetchall()
                response = {'list_level': list_level}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.LEVEL")
                list_level = cursor.fetchall()
                response = {'list_level': list_level}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_level.html', response)


def list_menggunakan_apparel(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)
    
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT username_pengguna, nama_tokoh, nama, warna_apparel, nama_pekerjaan, kategori_apparel FROM the_cims.menggunakan_apparel m JOIN the_cims.koleksi_jual_beli k ON m.id_koleksi = k.id_koleksi JOIN the_cims.apparel a ON k.id_koleksi = a.id_koleksi ORDER BY username_pengguna, nama_tokoh ASC")
                list_menggunakan_apparel = cursor.fetchall()
                response = {'list_menggunakan_apparel': list_menggunakan_apparel}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT m.id_koleksi, nama_tokoh, nama, warna_apparel, nama_pekerjaan, kategori_apparel FROM the_cims.menggunakan_apparel m JOIN the_cims.koleksi_jual_beli k ON m.id_koleksi = k.id_koleksi JOIN the_cims.apparel a ON k.id_koleksi = a.id_koleksi WHERE username_pengguna='{request.session['username']}' ORDER BY nama_tokoh ASC")
                list_menggunakan_apparel = cursor.fetchall()
                response = {'list_menggunakan_apparel': list_menggunakan_apparel}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_menggunakan_apparel.html', response)


def list_misi_utama(request):
    response = {}
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                SELECT *, case
                    when ( exists (
                            SELECT *
                            FROM the_cims.misi_utama as mu1, the_cims.menjalankan_misi_utama as mmu1
                            WHERE mu1.nama_misi = mmu1.nama_misi and mu2.nama_misi = mu1.nama_misi
                            )
                        )
                        then 0
                        else 1
                    end
                    as deletable
                
                FROM the_cims.misi_utama as mu2
                    left join the_cims.misi as m
                        on mu2.nama_misi = m.nama
                """)
                list_misi_utama = cursor.fetchall()
                for i in range(len(list_misi_utama)):
                    list_misi_utama[i] = list(list_misi_utama[i])
                    misi = list_misi_utama[i]
                    misi[8] = f"{misi[8].hour} jam, {misi[8].minute} menit, {misi[8].second} detik"
                response = {'list_misi_utama': list_misi_utama}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                                SELECT *, case
                                    when ( exists (
                                            SELECT *
                                            FROM the_cims.misi_utama as mu1, the_cims.menjalankan_misi_utama as mmu1
                                            WHERE mu1.nama_misi = mmu1.nama_misi and mu2.nama_misi = mu1.nama_misi
                                            )
                                        )
                                        then 0
                                        else 1
                                    end
                                    as deletable

                                FROM the_cims.misi_utama as mu2
                                    left join the_cims.misi as m
                                        on mu2.nama_misi = m.nama
                                """)
                list_misi_utama = cursor.fetchall()
                for i in range(len(list_misi_utama)):
                    list_misi_utama[i] = list(list_misi_utama[i])
                    misi = list_misi_utama[i]
                    misi[8] = f"{misi[8].hour} jam, {misi[8].minute} menit, {misi[8].second} detik"
                response = {'list_misi_utama': list_misi_utama}
        except Exception as e:
            messages.info(request, str(e))
    return render(request, 'list_misi_utama.html', response)


def list_menjalankan_misi_utama(request):
    response = {}
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                   SELECT *
                   FROM the_cims.menjalankan_misi_utama
                """)
                list_menjalankan_misi_utama = cursor.fetchall()
                response = {'list_menjalankan_misi_utama': list_menjalankan_misi_utama}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                   SELECT *
                   FROM the_cims.menjalankan_misi_utama as mmu
                   WHERE mmu.username_pengguna = '{request.session['username']}'
                                """)
                list_menjalankan_misi_utama = cursor.fetchall()
                response = {'list_menjalankan_misi_utama': list_menjalankan_misi_utama}
        except Exception as e:
            messages.info(request, str(e))
    return render(request, 'list_menjalankan_misi_utama.html', response)


def list_makanan(request):
    response = {}
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                   SELECT *, case
                    when ( exists (
                            SELECT *
                            FROM the_cims.makan as m
                            WHERE m.nama_makanan = mn.nama
                            )
                        )
                        then 0
                        else 1
                    end
                    as deleteable
                FROM the_cims.makanan as mn
                """)
                list_makanan = cursor.fetchall()
                response = {'list_makanan': list_makanan}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                   SELECT *
                   FROM the_cims.makanan
                                """)
                list_makanan = cursor.fetchall()
                response = {'list_makanan': list_makanan}
        except Exception as e:
            messages.info(request, str(e))
    return render(request, 'list_makanan.html', response)


def list_makan(request):
    response = {}
    if "isLogin" not in request.session:
        return login(request)
    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                   SELECT *
                   FROM the_cims.makan
                """)
                list_makan = cursor.fetchall()
                response = {'list_makan': list_makan}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                    SELECT *
                    FROM the_cims.makan
                    WHERE makan.username_pengguna = '{request.session['username']}'
                                """)
                list_makan = cursor.fetchall()
                response = {'list_makan': list_makan}
        except Exception as e:
            messages.info(request, str(e))
    return render(request, 'list_makan.html', response)


def list_kategori_apparel(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT nama_kategori, CASE
                                     WHEN nama_kategori NOT IN (SELECT kategori_apparel
                                                                  FROM THE_CIMS.apparel)
                                         THEN 'true'
                                     ELSE 'false'
                                     END CAN_DELETE
                                 FROM THE_CIMS.kategori_apparel""")
                list_kategori_apparel = cursor.fetchall()
                response = {'list_kategori_apparel': list_kategori_apparel}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM THE_CIMS.kategori_apparel")
                list_kategori_apparel = cursor.fetchall()
                response = {'list_kategori_apparel': list_kategori_apparel}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_kategori_apparel.html', response)


def list_koleksi_tokoh(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT nama_tokoh, id_koleksi, CASE
                                      WHEN id_koleksi NOT IN (SELECT id_mata
                                             FROM THE_CIMS.tokoh)

                                       and id_koleksi NOT IN (SELECT id_rumah
                                             FROM THE_CIMS.tokoh)

                                       and id_koleksi NOT IN (SELECT id_rambut
                                             FROM THE_CIMS.tokoh)

                                      and id_koleksi NOT IN (SELECT id_barang
                                             FROM THE_CIMS.menggunakan_barang)

                                      and id_koleksi NOT IN (SELECT id_koleksi
                                             FROM THE_CIMS.menggunakan_apparel)
                                             THEN 'true'
                                         ELSE 'false'
                                         END deletable
                                   FROM THE_CIMS.koleksi_tokoh""")
                list_koleksi_tokoh = cursor.fetchall()
                response = {'list_koleksi_tokoh': list_koleksi_tokoh}
        except Exception as e:
            messages.info(request, str(e))
    elif request.session['role'] == 'Admin':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT * FROM THE_CIMS.koleksi_tokoh""")
                list_koleksi_tokoh = cursor.fetchall()
                response = {'list_koleksi_tokoh': list_koleksi_tokoh}
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)
    return render(request, 'list_koleksi_tokoh.html', response)


def delete_koleksi(request):
    if "isLogin" not in request.session:
            return login(request)

    if request.session['role'] == 'Admin':
        koleksi = request.session['koleksi']
    cursor = connection.cursor()
    cursor.execute(f"DELETE FROM the_cims.koleksi where id_koleksi={koleksi}")
    return redirect('/read/list_koleksi')

def list_koleksi(request):
    response = {}

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            if request.POST['delete_koleksi'] != None:
                request.session['koleksi'] = request.POST['delete_koleksi']
                return ""

            if request.POST['update_koleksi'] != None:
                return ""


        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT id, harga, tipe, CASE WHEN id NOT IN (SELECT id_rambut FROM the_cims.TOKOH) THEN 'true' ELSE 'false'
                                  END deletable
                                  FROM the_cims.rambut left join the_cims.koleksi
                                  on rambut.id_koleksi = koleksi.id""")
                rambut = cursor.fetchall()
                response['rambut'] = rambut


                cursor.execute(f"""SELECT id, harga, warna, CASE WHEN id NOT IN (SELECT id_mata FROM the_cims.TOKOH) THEN 'true' ELSE 'false'
                                   END AS deletable
                                   FROM the_cims.mata left join the_cims.koleksi
                                   on mata.id_koleksi = koleksi.id""")
                mata = cursor.fetchall()
                response['mata'] = mata

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, kapasitas_barang, CASE WHEN id NOT IN (SELECT id_rumah FROM the_cims.TOKOH) THEN 'true' ELSE 'false'
                                   END AS deletable
                                   FROM the_cims.rumah left join the_cims.koleksi
                                   on rumah.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on rumah.id_koleksi = koleksi_jual_beli.id_koleksi""")
                rumah = cursor.fetchall()
                response['rumah'] = rumah

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, tingkat_energi, CASE WHEN id NOT IN (SELECT id_barang FROM the_cims.menggunakan_barang) THEN 'true' ELSE 'false'
                                   END AS deletable
                                   FROM the_cims.barang left join the_cims.koleksi
                                   on barang.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on barang.id_koleksi = koleksi_jual_beli.id_koleksi""")
                barang = cursor.fetchall()
                response['barang'] = barang

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, kategori_apparel, nama_pekerjaan, CASE WHEN id NOT IN (SELECT id_koleksi FROM the_cims.menggunakan_apparel) THEN 'true' ELSE 'false'
                                   END AS deletable
                                   FROM the_cims.apparel left join the_cims.koleksi
                                   on apparel.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on apparel.id_koleksi = koleksi_jual_beli.id_koleksi""")
                apparel = cursor.fetchall()
                response['apparel'] = apparel
                print(apparel)
        except Exception as e:
            messages.info(request, str(e))

    elif request.session['role'] == 'Pemain':
        try:
            with connection.cursor() as cursor:
                dictionary = dict()
                cursor.execute(f"""SELECT id, harga, tipe
                                   FROM the_cims.rambut left join the_cims.koleksi
                                   on rambut.id_koleksi = koleksi.id""")
                rambut = cursor.fetchall()
                response['rambut'] = rambut

                cursor.execute(f"""SELECT id, harga, warna
                                   FROM the_cims.mata left join the_cims.koleksi
                                   on mata.id_koleksi = koleksi.id""")
                mata = cursor.fetchall()
                response['mata'] = mata

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, kapasitas_barang
                                   FROM the_cims.rumah left join the_cims.koleksi
                                   on rumah.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on rumah.id_koleksi = koleksi_jual_beli.id_koleksi""")
                rumah = cursor.fetchall()
                response['rumah'] = rumah

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, tingkat_energi
                                   FROM the_cims.barang left join the_cims.koleksi
                                   on barang.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on barang.id_koleksi = koleksi_jual_beli.id_koleksi""")
                barang = cursor.fetchall()
                response['barang'] = barang

                cursor.execute(f"""SELECT id, nama, harga, harga_beli, kategori_apparel, nama_pekerjaan
                                   FROM the_cims.apparel left join the_cims.koleksi
                                   on apparel.id_koleksi = koleksi.id
                                   left join the_cims.koleksi_jual_beli
                                   on apparel.id_koleksi = koleksi_jual_beli.id_koleksi""")
                apparel = cursor.fetchall()
                response['apparel'] = apparel
        except Exception as e:
            messages.info(request, str(e))
    else:
        return login(request)

    return render(request, 'list_koleksi.html', response)


def delete_kategori_apparel(request, nama):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from kategori_apparel where nama_kategori = %s', [nama])
    return redirect('/read/list_kategori_apparel')

def hapus_kategori_apparel(request, nama):
    nama_kategori = nama
    print(nama_kategori)
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        try :
            cursor = connection.cursor()
            cursor.execute("DELETE FROM the_cims.kategori_apparel where nama_kategori ='"+nama_kategori+"'")
        except Exception as e:
            print(str(e))
        return redirect('/read/list_kategori_apparel')
    elif request.session['role'] == 'Pemain':
        return redirect('/')
    else:
        return login(request)
    return redirect('/read/list_kategori_apparel')

def update_rambut(request, id_, harga, tipe):
    cursor = connection.cursor()
    print(harga)
    form = UpdateRambutForm(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        cursor.execute('set search_path to the_cims')
        harga_jual = form.cleaned_data['harga_jual']
        tipe_updated = form.cleaned_data['tipe']
        cursor.execute('update koleksi set harga = %s where id = %s', [harga_jual, id_])
        cursor.execute('update rambut set tipe = %s where id_koleksi = %s',[tipe_updated, id_])
        return redirect('/read/list_koleksi')
    return render(request, 'update_rambut.html', {'id':id_, 'harga_jual':harga, 'tipe':tipe})

def update_mata(request, id_, harga, warna):
    cursor = connection.cursor()
    print(harga)
    form = UpdateMataForm(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        cursor.execute('set search_path to the_cims')
        harga_jual = form.cleaned_data['harga_jual']
        warna_updated = form.cleaned_data['warna']
        cursor.execute('update koleksi set harga = %s where id = %s', [harga_jual, id_])
        cursor.execute('update mata set warna = %s where id_koleksi = %s',[warna_updated, id_])
        return redirect('/read/list_koleksi')
    return render(request, 'update_mata.html', {'id':id_, 'harga_jual':harga, 'warna':warna})

def update_rumah(request, id_, nama, harga_jual, harga_beli, kapasitas_barang):
    cursor = connection.cursor()
    form = UpdateRambutForm(request.POST or None)
    print("yeay")
    if (request.method == "POST"):
        cursor.execute('set search_path to the_cims')
        name = request.POST.get('nama')
        harga_jual_updated = request.POST.get('harga_jual')
        harga_updated = request.POST.get('harga_beli')
        kapasitas_barang_updated = request.POST.get('kapasitas_barang')
        cursor.execute('update koleksi set harga = %s where id = %s', [harga_jual_updated, id_])
        cursor.execute('update koleksi_jual_beli set harga_beli = %s, nama = %s where id_koleksi = %s', [harga_updated, name, id_])
        cursor.execute('update rumah set kapasitas_barang = %s where id_koleksi = %s',[kapasitas_barang_updated, id_])
        return redirect('/read/list_koleksi')
    return render(request, 'update_rumah.html', {'id':id_,'name':nama,'harga_jual':harga_jual,'harga_beli':harga_beli,'kapasitas_barang':kapasitas_barang})

def update_barang(request, id_, nama, harga_jual, harga_beli, tingkat_energi):
    cursor = connection.cursor()
    form = UpdateBarangForm(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        cursor.execute('set search_path to the_cims')
        name = form.cleaned_data['nama']
        harga_jual_updated = form.cleaned_data['harga_jual']
        harga_updated = form.cleaned_data['harga_beli']
        tingkat_energi_updated = form.cleaned_data['tingkat_energi']
        cursor.execute('update koleksi set harga = %s where id = %s', [harga_jual_updated, id_])
        cursor.execute('update koleksi_jual_beli set harga_beli = %s, nama = %s where id_koleksi = %s', [harga_updated, name, id_])
        cursor.execute('update barang set tingkat_energi = %s where id_koleksi = %s',[tingkat_energi_updated, id_])
        return redirect('/read/list_koleksi')
    return render(request, 'update_barang.html', {'id':id_,'name':nama,'harga_jual':harga_jual,'harga_beli':harga_beli,'tingkat_energi':tingkat_energi})

def update_apparel(request, id_, nama, harga_jual, harga_beli):
    cursor = connection.cursor()
    if "isLogin" not in request.session:
            return login(request)

    if request.session['role'] == 'Admin':
        form = UpdateApparelForm(request.POST or None)
        cursor.execute('set search_path to the_cims')
    if (request.method == "POST"):
        name = request.POST.get('nama')
        harga_jual_updated = request.POST.get('harga_jual')
        harga_updated = request.POST.get('harga_beli')
        warna = request.POST.get('warna')
        kategori_apparel_updated = request.POST.get('kategori_apparel')
        np = request.POST.get('nama_pekerjaan')

        cursor.execute('update koleksi set harga = %s where id = %s', [harga_jual_updated, id_])
        cursor.execute('update koleksi_jual_beli set harga_beli = %s, nama = %s where id_koleksi = %s', [harga_updated, name, id_])
        cursor.execute('update apparel set nama_pekerjaan = %s, kategori_apparel = %s, warna_apparel=%s where id_koleksi = %s',[np, kategori_apparel_updated, warna, id_])
        return redirect('/read/list_koleksi')
    cursor.execute('select * from kategori_apparel')
    kategori = namedtuplefetchall(cursor)
    cursor.execute('select * from pekerjaan')
    nama_pekerjaan = namedtuplefetchall(cursor)
    return render(request, 'update_apparel.html', {'id':id_,'name':nama,'harga_jual':harga_jual,'harga_beli':harga_beli,'kategori_apparel':kategori, 'nama_pekerjaan':nama_pekerjaan})

def delete_rambut(request, id_):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from rambut where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi where id = %s', [id_])
    return redirect('/read/list_koleksi')

def delete_mata(request, id_):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from mata where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi where id = %s', [id_])
    return redirect('/read/list_koleksi')


@transaction.atomic
def delete_rumah(request, id_):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from rumah where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi_jual_beli where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi where id = %s', [id_])
    return redirect('/read/list_koleksi')

def delete_barang(request, id_):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from barang where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi_jual_beli where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi where id = %s', [id_])
    return redirect('/read/list_koleksi')

def delete_apparel(request, id_):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from apparel where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi_jual_beli where id_koleksi = %s', [id_])
    cursor.execute('delete from koleksi where id = %s', [id_])
    return redirect('/read/list_koleksi')

def delete_koleksi_tokoh(request, id_, nama):
    cursor = connection.cursor()
    cursor.execute('set search_path to the_cims')
    cursor.execute('delete from koleksi_tokoh where id_koleksi = %s and nama_tokoh = %s', [id_, nama])
    return redirect('/read/list_koleksi_tokoh')
