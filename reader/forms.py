from django import forms
from django.db import connection

class UpdateRambutForm(forms.Form):
    harga_jual = forms.IntegerField()
    tipe=forms.CharField(label='tipe', max_length=50)

class UpdateMataForm(forms.Form):
    harga_jual = forms.IntegerField()
    warna = forms.CharField(label='warna', max_length=50)

class UpdateRumahForm(forms.Form):
    nama = forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    kapasitas_barang=forms.IntegerField()

class UpdateApparelForm(forms.Form):
    nama=forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    warna=forms.CharField(label='warna', max_length=50)
    # kategori_apparel=forms.CharField(label='kategori_apparel', widget=forms.Select(choices=get_kategori_apparel()))
    kategori_apparel=forms.CharField(label='kategori_apparel')
    nama_pekerjaan=forms.CharField(label='nama_pekerjaan')

class UpdateBarangForm(forms.Form):
    nama=forms.CharField(label='nama', max_length=50)
    harga_jual=forms.IntegerField()
    harga_beli = forms.IntegerField()
    tingkat_energi=forms.IntegerField()
