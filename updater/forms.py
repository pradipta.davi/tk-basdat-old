from django import forms

class FormTokoh(forms.Form):
    def __init__(self, *args, **kwargs):
        self.rambut_options = kwargs.pop('rambut_options')
        self.mata_options = kwargs.pop('mata_options')
        self.rumah_options = kwargs.pop('rumah_options')
        super(FormTokoh, self).__init__(*args, **kwargs)
        self.fields['rambut'].widget = forms.Select(choices=self.rambut_options)
        self.fields['mata'].widget = forms.Select(choices=self.mata_options)
        self.fields['rumah'].widget = forms.Select(choices=self.rumah_options)
    nama = forms.CharField(label='Nama', disabled=True)
    rambut = forms.CharField(label="ID Rambut")
    mata = forms.CharField(label="ID Mata")
    rumah = forms.CharField(label="ID Rumah")

class FormLevel(forms.Form):
    level = forms.IntegerField(label='Level', disabled=True)
    xp = forms.IntegerField(label='XP')


class FormUpdateMenjalankanMisiUtama(forms.Form):
    nama_tokoh = forms.CharField(label='nama_tokoh', disabled=True)
    nama_misi_utama = forms.CharField(label='nama_misi_utama', disabled=True)
    status = forms.CharField(label='status')


class FormUpdateMakanan(forms.Form):
    nama_makanan = forms.CharField(label='nama_makanan', disabled=True)
    harga = forms.IntegerField(label='harga')
    tingkat_energi = forms.IntegerField(label='tingkat_energi')
    tingkat_kelaparan = forms.IntegerField(label='tingkat_kelaparan')
