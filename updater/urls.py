from django.urls import path
from . import views
app_name = 'updater'
urlpatterns = [
    path('update_level/<int:level>', views.update_level, name='update_level'),
    path('update_tokoh/<str:nama>', views.update_tokoh, name='update_tokoh'),
    path('update_menjalankan_misi_utama', views.update_menjalankan_misi_utama, name='update_menjalankan_misi_utama'),
    path('update_makanan', views.update_makanan, name='update_makanan'),
]