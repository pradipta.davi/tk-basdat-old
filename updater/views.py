from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import connection
from django.http import HttpResponseRedirect
from .forms import *
from authentication.views import login


def update_tokoh(request, nama):
    if "isLogin" not in request.session:
        return login(request)

    response = {}

    if (request.session['role'] == 'Admin'):
        messages.info(request, 'This page is restricted to players only.')
    elif (request.session['role'] == 'Pemain'):
        if request.method == 'POST':
            message = ""
            input_rambut = request.POST['rambut']
            input_mata = request.POST['mata']
            input_rumah = request.POST['rumah']
            cursor = connection.cursor()
            try:
                cursor.execute(
                    f"UPDATE THE_CIMS.TOKOH SET id_rambut='{input_rambut}', id_mata='{input_mata}', id_rumah='{input_rumah}' WHERE nama='{nama}' AND username_pengguna='{request.session['username']}'")
                cursor.close()
                return redirect('/read/list_tokoh')
            except Exception as e:
                messages.info(request, str(e))
        list_rambut = []
        list_mata = []
        list_rumah = []
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT id_koleksi, id_koleksi FROM THE_CIMS.koleksi_tokoh WHERE username_pengguna='{request.session['username']}' AND nama_tokoh='{nama}' AND id_koleksi LIKE 'RB%'")
                list_rambut = cursor.fetchall()
                cursor.execute(
                    f"SELECT id_koleksi, id_koleksi FROM THE_CIMS.koleksi_tokoh WHERE username_pengguna='{request.session['username']}' AND nama_tokoh='{nama}' AND id_koleksi LIKE 'MT%'")
                list_mata = cursor.fetchall()
                cursor.execute(
                    f"SELECT id_koleksi, id_koleksi FROM THE_CIMS.koleksi_tokoh WHERE username_pengguna='{request.session['username']}' AND nama_tokoh='{nama}' AND id_koleksi LIKE 'RM%'")
                list_rumah = cursor.fetchall()
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormTokoh(request.POST or None, initial={'nama': nama}, rambut_options=list_rambut,
                         mata_options=list_mata, rumah_options=list_rumah)
        response['form'] = form
    else:
        return login(request)

    return render(request, 'update_tokoh.html', response)


def update_level(request, level):
    if "isLogin" not in request.session:
        return login(request)

    response = {}

    if (request.session['role'] == 'Admin'):
        xp = 0
        if request.method == 'POST':
            message = ""
            input_xp = request.POST['xp']
            cursor = connection.cursor()
            try:
                cursor.execute(f"UPDATE THE_CIMS.LEVEL SET xp={input_xp} WHERE level={level}")
                cursor.close()
                return redirect('/read/list_level')
            except Exception as e:
                messages.info(request, str(e))
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT xp FROM THE_CIMS.LEVEL WHERE level='{level}'")
                xp = cursor.fetchone()[0]
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormLevel(request.POST or None, initial={'level': level, 'xp': xp})
        response['form'] = form
    elif (request.session['role'] == 'Pemain'):
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)

    return render(request, 'update_level.html', response)


def update_menjalankan_misi_utama(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        messages.info(request, 'This page is restricted to players only.')
    elif request.session['role'] == 'Pemain':
        if request.method == 'POST':
            username_pengguna = request.session['username']
            nama_tokoh = request.GET.get("nama_tokoh")
            nama_misi_utama = request.GET.get("nama_misi")
            status = request.POST.get('status')
            try:
                with connection.cursor() as cursor:
                    cursor.execute("""
                    UPDATE THE_CIMS.menjalankan_misi_utama
                    SET status = %s
                    WHERE (username_pengguna, nama_tokoh, nama_misi) = (%s, %s, %s)
                    """, [status, username_pengguna, nama_tokoh, nama_misi_utama])
                    cursor.close()
                    return redirect('/read/list_menjalankan_misi_utama')
            except Exception as e:
                messages.info(request, str(e))
        nama_tokoh = request.GET.get("nama_tokoh")
        nama_misi = request.GET.get("nama_misi")
        form = FormUpdateMenjalankanMisiUtama(request.POST or None, initial={'nama_tokoh': nama_tokoh,
                                                                             'nama_misi_utama': nama_misi})
        response['form'] = form
    else:
        return login(request)
    return render(request, 'update_menjalankan_misi_utama.html', response)


def update_makanan(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            nama_makanan = request.GET['nama_makanan']
            harga = request.POST['harga']
            tingkat_energi = request.POST['tingkat_energi']
            tingkat_kelaparan = request.POST['tingkat_kelaparan']
            try:
                with connection.cursor() as cursor:
                    cursor.execute("""
                    UPDATE THE_CIMS.makanan
                    SET harga = %s, tingkat_energi = %s, tingkat_kelaparan = %s
                    WHERE nama = (%s)
                    """, [harga, tingkat_energi, tingkat_kelaparan, nama_makanan])
                    cursor.close()
                    return redirect('/read/list_makanan')
            except Exception as e:
                messages.info(request, str(e))
        nama_makanan = request.GET.get("nama_makanan")
        form = FormUpdateMakanan(request.POST or None, initial={'nama_makanan': nama_makanan})
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'update_makanan.html', response)
