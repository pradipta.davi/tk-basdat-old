from django.urls import path
from . import views
app_name = 'creator'
urlpatterns = [
    path('buat_tokoh', views.buat_tokoh, name='buat_tokoh'),
    path('buat_warna_kulit', views.buat_warna_kulit, name='buat_warna_kulit'),
    path('buat_level', views.buat_level, name='buat_level'),
    path('buat_menggunakan_apparel', views.buat_menggunakan_apparel, name='buat_menggunakan_apparel'),
    path('buat_misi_utama', views.buat_misi_utama, name='buat_misi_utama'),
    path('buat_menjalankan_misi_utama', views.buat_menjalankan_misi_utama, name='buat_menjalankan_misi_utama'),
    path('buat_makanan', views.buat_makanan, name='buat_makanan'),
    path('buat_makan', views.buat_makan, name='buat_makan'),
    path('buat_kategori_apparel', views.buat_kategori_apparel, name='buat_kategori_apparel'),
    path('buat_rambut', views.buat_rambut, name='buat_rambut'),
    path('pilih_apparel/<str:tokoh>', views.pilih_apparel, name='pilih_apparel'),
    path('buat_koleksi', views.buat_koleksi, name='buat_koleksi'),
    path('buat_mata', views.buat_mata, name='buat_mata'),
    path('buat_rumah', views.buat_rumah, name='buat_rumah'),
    path('buat_barang', views.buat_barang, name='buat_barang'),
    path('buat_apparel', views.buat_apparel, name='buat_apparel'),
    path('buat_koleksi_tokoh',views.buat_koleksi_tokoh,name='buat_koleksi_tokoh'),
]
