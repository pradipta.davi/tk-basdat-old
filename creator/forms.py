from django import forms

class FormTokoh(forms.Form):
    def __init__(self, *args, **kwargs):
        self.jenis_kelamin_options = kwargs.pop('jenis_kelamin_options')
        self.warna_kulit_options = kwargs.pop('warna_kulit_options')
        self.pekerjaan_options = kwargs.pop('pekerjaan_options')
        super(FormTokoh, self).__init__(*args, **kwargs)
        self.fields['jenis_kelamin'].widget = forms.Select(choices=self.jenis_kelamin_options)
        self.fields['warna_kulit'].widget = forms.Select(choices=self.warna_kulit_options)
        self.fields['pekerjaan'].widget = forms.Select(choices=self.pekerjaan_options)
    nama = forms.CharField(label='Nama', max_length=50)
    jenis_kelamin = forms.CharField(label='Jenis Kelamin')
    warna_kulit = forms.CharField(label='Warna Kulit')
    pekerjaan = forms.CharField(label='Pekerjaan')

class FormWarnaKulit(forms.Form):
    kode = forms.CharField(label='Kode', max_length=10)


class FormLevel(forms.Form):
    level = forms.IntegerField(label='Level')
    xp = forms.IntegerField(label='XP')


class FormMenggunakanApparel(forms.Form):
    def __init__(self, *args, **kwargs):
        self.tokoh_options = kwargs.pop('tokoh_options')
        super(FormMenggunakanApparel, self).__init__(*args, **kwargs)
        self.fields['tokoh'].widget = forms.Select(choices=self.tokoh_options)
    tokoh = forms.CharField(label='Nama Tokoh')

class FormPilihApparel(forms.Form):
    def __init__(self, *args, **kwargs):
        self.apparel_options = kwargs.pop('apparel_options')
        super(FormPilihApparel, self).__init__(*args, **kwargs)
        self.fields['apparel'].widget = forms.Select(choices=self.apparel_options)
    apparel = forms.CharField(label='Apparel')


class FormMisiUtama(forms.Form):
    nama_misi_utama = forms.CharField(label='Nama Misi Utama', max_length=50)
    efek_energi = forms.IntegerField(label='Efek Energi')
    efek_hubungan_sosial = forms.IntegerField(label='Efek Hubungan Sosial')
    efek_kelaparan = forms.IntegerField(label='Efek Kelaparan')
    syarat_energi = forms.IntegerField(label='Syarat Energi')
    syarat_hubungan_sosial = forms.IntegerField(label='Syarat Hubungan Sosial')
    syarat_kelaparan = forms.IntegerField(label='Syarat Kelaparan')
    completion_time = forms.TimeField(label='Completion Time')
    reward_koin = forms.IntegerField(label='Reward Koin')
    reward_xp = forms.IntegerField(label='Reward XP')
    deskripsi = forms.CharField(label='Deskripsi', widget=forms.Textarea)


class FormMenjalankanMisiUtama(forms.Form):
    nama_tokoh = forms.CharField(label='Nama Tokoh', max_length=50)
    nama_misi_utama = forms.CharField(label='Nama Misi Utama', max_length=50)


class FormMakanan(forms.Form):
    nama_makanan = forms.CharField(label='Nama Makanan', max_length=50)
    harga = forms.IntegerField(label='Harga')
    tingkat_energi = forms.IntegerField(label='Tingkat Energi')
    tingkat_kelaparan = forms.IntegerField(label='Tingkat Kelaparan')


class FormMakan(forms.Form):
    nama_tokoh = forms.CharField(label='Nama Tokoh', max_length=50)
    nama_makanan = forms.CharField(label='Nama Makanan', max_length=50)


class FormKategoriApparel(forms.Form):
    nama_kategori = forms.CharField(label='Nama Kategori', max_length=10)

class FormBuatRambut(forms.Form):
    # new_id=forms.CharField(label='new_id',max_length=50)
    harga_jual = forms.IntegerField()
    tipe = forms.CharField(label='tipe', max_length=50)

class FormBuatMata(forms.Form):
    harga_jual = forms.IntegerField()
    warna=forms.CharField(label='warna', max_length=50)

class FormBuatRumah(forms.Form):
    nama=forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    kapasitas_barang=forms.IntegerField()

class FormBuatBarang(forms.Form):
    nama=forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    tingkat_energi=forms.IntegerField()

class FormBuatApparel(forms.Form):
    nama=forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    warna=forms.CharField(label='warna', max_length=50)

class BuatFormApparel(forms.Form):
    nama=forms.CharField(label='nama',max_length=50)
    harga_jual = forms.IntegerField()
    harga_beli = forms.IntegerField()
    warna=forms.CharField(label='warna', max_length=50)
    # kategori_apparel=forms.CharField(label='kategori_apparel', widget=forms.Select(choices=get_kategori_apparel()))
    kategori_apparel=forms.CharField(label='kategori_apparel')
    nama_pekerjaan=forms.CharField(label='nama_pekerjaan')

class CreateKoleksiTokoh(forms.Form):
    nama=forms.CharField(label='nama', max_length=50)
    koleksi=forms.CharField(label='koleksi', max_length=50)