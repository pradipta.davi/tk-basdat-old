import datetime
from random import randrange
from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import connection, transaction
from .forms import *
from authentication.views import login
from django.http.response import HttpResponseNotFound, HttpResponseRedirect
from django.shortcuts import redirect, render
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
def buat_tokoh(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        messages.info(request, 'This page is restricted to players only.')
    elif request.session['role'] == 'Pemain':
        """input = request.POST['kode']
        cursor = connection.cursor()
        try :
            cursor.execute("INSERT INTO THE_CIMS.WARNA_KULIT(kode) VALUES ('"+input+"')")
            cursor.close()
            return redirect('/read/list_warna_kulit')
        except Exception as e:
            message = str(e)
            if "Skin" in message :
                message = message[:57]
                messages.info(request, message)"""
        if request.method == 'POST':
            message = ""
            input_nama = request.POST['nama']
            input_jenis_kelamin = request.POST['jenis_kelamin']
            input_warna_kulit = request.POST['warna_kulit']
            input_pekerjaan = request.POST['pekerjaan']
            list_sifat = []
            cursor = connection.cursor()
            try:
                with connection.cursor() as cursor:
                    cursor.execute(f"SELECT nama_sifat FROM THE_CIMS.sifat")
                    list_sifat = cursor.fetchall()
                    sifat = list_sifat[randrange(len(list_sifat))][0]
                    cursor.execute(f"""INSERT INTO the_cims.tokoh(username_pengguna, nama, jenis_kelamin, warna_kulit, sifat, pekerjaan)
                    VALUES ('{request.session['username']}', '{input_nama}', '{input_jenis_kelamin}', '{input_warna_kulit}', '{sifat}',
                    '{input_pekerjaan}')""")
                    cursor.close()
                return redirect('/read/list_tokoh')
            except Exception as e:
                messages.info(request, str(e))
        list_jenis_kelamin = [('Laki-laki', 'Laki-laki'), ('Perempuan', 'Perempuan')]
        list_warna_kulit = []
        list_pekerjaan = []
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT kode, kode FROM THE_CIMS.warna_kulit")
                list_warna_kulit = cursor.fetchall()
                cursor.execute(
                    f"SELECT nama, nama FROM THE_CIMS.pekerjaan")
                list_pekerjaan = cursor.fetchall()
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormTokoh(request.POST or None, jenis_kelamin_options=list_jenis_kelamin,
                         warna_kulit_options=list_warna_kulit, pekerjaan_options=list_pekerjaan)
        response['form'] = form
    else:
        return login(request)
    return render(request, 'buat_tokoh.html', response)


def buat_warna_kulit(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            message = ""
            input = request.POST['kode']
            cursor = connection.cursor()
            try:
                cursor.execute("INSERT INTO THE_CIMS.WARNA_KULIT(kode) VALUES ('" + input + "')")
                cursor.close()
                return redirect('/read/list_warna_kulit')
            except Exception as e:
                message = str(e)
                if "Skin" in message:
                    message = message[:57]
                messages.info(request, message)
        response['error'] = False
        form = FormWarnaKulit(request.POST or None)
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'buat_warna_kulit.html', response)


def buat_level(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            message = ""
            input_level = request.POST['level']
            input_xp = request.POST['xp']
            cursor = connection.cursor()
            try:
                cursor.execute(
                    "INSERT INTO THE_CIMS.LEVEL(level, xp) VALUES ('" + input_level + "','" + input_xp + "')")
                cursor.close()
                return redirect('/read/list_level')
            except Exception as e:
                message = str(e)
                messages.info(request, message)
        response['error'] = False
        form = FormLevel(request.POST or None)
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'buat_level.html', response)


def buat_kategori_apparel(request):
    cursor = connection.cursor()
    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        print ("masuk admin")

        cursor.execute("SET SEARCH_PATH TO the_cims")
        if request.method == "POST":
            print("masuk post")
            nama_kategori = request.POST.get("nama_kategori")
            print(nama_kategori)
            if not nama_kategori:
                message = "Data yang diisikan belum lengkap, silahkan lengkapi data terlebih dahulu."
                return render(request, 'buat_kategori_apparel.html', {'message':message})
            else:
                cursor.execute(f"INSERT INTO kategori_apparel VALUES ('{nama_kategori}')")
                return redirect("/read/list_kategori_apparel")
        else:
            return render(request, 'buat_kategori_apparel.html')
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)


def buat_koleksi(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        response['error'] = False
        form = FormKategoriApparel(request.POST or None)
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'buat_koleksi.html', response)


def buat_menggunakan_apparel(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if (request.session['role'] == 'Admin'):
        messages.info(request, 'This page is restricted to players only.')
    elif (request.session['role'] == 'Pemain'):
        if request.method == 'POST':
            message = ""
            input = request.POST['tokoh']
            url = '/create/pilih_apparel/' + input
            return redirect(url)
        list_tokoh = []
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT nama, nama FROM THE_CIMS.TOKOH WHERE username_pengguna='{request.session['username']}'")
                list_tokoh = cursor.fetchall()
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormMenggunakanApparel(request.POST or None, tokoh_options=list_tokoh)
        response['form'] = form
    else:
        return login(request)
    return render(request, 'buat_menggunakan_apparel.html', response)


def pilih_apparel(request, tokoh):
    response = {}
    if request.method == 'POST':
        message = ""
        apparel = request.POST['apparel']
        cursor = connection.cursor()
        try:
            cursor.execute(
                f"INSERT INTO the_cims.menggunakan_apparel(username_pengguna, nama_tokoh, id_koleksi) VALUES ('{request.session['username']}', '{tokoh}', '{apparel}')")
            cursor.close()
            return redirect('/read/list_menggunakan_apparel')
        except Exception as e:
            message = str(e)
            messages.info(request, message)
    response['tokoh'] = tokoh
    list_apparel = []
    try:
        with connection.cursor() as cursor:
            cursor.execute(
                f"SELECT id_koleksi, id_koleksi FROM the_cims.koleksi_tokoh WHERE id_koleksi LIKE 'AP%' AND nama_tokoh = '{tokoh}'")
            list_apparel = cursor.fetchall()
    except Exception as e:
        messages.info(request, str(e))
    response['error'] = False
    form = FormPilihApparel(request.POST or None, apparel_options=list_apparel)
    response['form'] = form
    return render(request, 'pilih_apparel.html', response)


@transaction.atomic
def buat_misi_utama(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            nama_misi_utama = request.POST['nama_misi_utama']
            efek_energi = request.POST['efek_energi']
            efek_hubungan_sosial = request.POST['efek_hubungan_sosial']
            efek_kelaparan = request.POST['efek_kelaparan']
            syarat_energi = request.POST['syarat_energi']
            syarat_hubungan_sosial = request.POST['syarat_hubungan_sosial']
            syarat_kelaparan = request.POST['syarat_kelaparan']
            completion_time = request.POST['completion_time']
            reward_koin = request.POST['reward_koin']
            reward_xp = request.POST['reward_xp']
            deskripsi = request.POST['deskripsi']

            cursor = connection.cursor()
            try:
                cursor.execute("INSERT INTO THE_CIMS.MISI VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                               [nama_misi_utama, efek_energi, efek_hubungan_sosial, efek_kelaparan, syarat_energi,
                                syarat_hubungan_sosial, syarat_kelaparan, completion_time, reward_koin, reward_xp,
                                deskripsi])
                cursor.execute("INSERT INTO THE_CIMS.misi_utama VALUES (%s)", [nama_misi_utama])
                cursor.close()
                return redirect('/read/list_misi_utama')
            except Exception as e:
                message = str(e)
                messages.info(request, message)

        response['error'] = False
        form = FormMisiUtama(request.POST or None)
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'buat_misi_utama.html', response)


def buat_menjalankan_misi_utama(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        messages.info(request, 'This page is restricted to players only.')
    elif request.session['role'] == 'Pemain':
        if request.method == 'POST':
            nama_tokoh = request.POST['nama_tokoh']
            nama_misi_utama = request.POST['nama_misi_utama']
            try:
                with connection.cursor() as cursor:
                    cursor.execute("""
                        INSERT INTO THE_CIMS.menjalankan_misi_utama 
                        VALUES (%s, %s, %s, %s)
                    """, [request.session['username'], nama_tokoh, nama_misi_utama, "In Progress"])
                    return redirect('/read/list_menjalankan_misi_utama')
            except Exception as e:
                messages.info(request, str(e))
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                                SELECT nama
                                FROM the_cims.tokoh
                                WHERE username_pengguna = '{request.session['username']}'
                                """)
                list_tokoh = cursor.fetchall()
                response['list_tokoh'] = list_tokoh
                cursor.execute(f"""
                                SELECT nama_misi
                                FROM the_cims.misi_utama
                                """)
                list_misi_utama = cursor.fetchall()
                response['list_misi_utama'] = list_misi_utama
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormMenjalankanMisiUtama(request.POST or None)
        response['form'] = form
    else:
        return login(request)
    return render(request, 'buat_menjalankan_misi_utama.html', response)


def buat_makanan(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        if request.method == 'POST':
            nama_makanan = request.POST['nama_makanan']
            harga = request.POST['harga']
            tingkat_energi = request.POST['tingkat_energi']
            tingkat_kelaparan = request.POST['tingkat_kelaparan']
            try:
                with connection.cursor() as cursor:
                    cursor.execute("""
                                INSERT INTO THE_CIMS.makanan 
                                VALUES (%s, %s, %s, %s)
                            """, [nama_makanan, harga, tingkat_energi, tingkat_kelaparan])
                    return redirect('/read/list_makanan')
            except Exception as e:
                messages.info(request, str(e))

        response['error'] = False
        form = FormMakanan(request.POST or None)
        response['form'] = form
    elif request.session['role'] == 'Pemain':
        messages.info(request, 'This page is restricted to admins only.')
    else:
        return login(request)
    return render(request, 'buat_makanan.html', response)


def buat_makan(request):
    if "isLogin" not in request.session:
        return login(request)
    response = {}
    if request.session['role'] == 'Admin':
        messages.info(request, 'This page is restricted to players only.')
    elif request.session['role'] == 'Pemain':
        if request.method == 'POST':
            username_pemain = request.session['username']
            nama_tokoh = request.POST['nama_tokoh']
            nama_makanan = request.POST['nama_makanan']
            try:
                with connection.cursor() as cursor:
                    cursor.execute("""
                        INSERT INTO THE_CIMS.makan 
                        VALUES (%s, %s, %s, %s)
                    """, [username_pemain, nama_tokoh, datetime.datetime.now(), nama_makanan])
                    return redirect('/read/list_makan')
            except Exception as e:
                messages.info(request, str(e))
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                                SELECT nama
                                FROM the_cims.tokoh
                                WHERE username_pengguna = '{request.session['username']}'
                                """)
                list_tokoh = cursor.fetchall()
                response['list_tokoh'] = list_tokoh
                cursor.execute(f"""
                                SELECT nama
                                FROM the_cims.makanan
                                """)
                list_makanan = cursor.fetchall()
                response['list_makanan'] = list_makanan
        except Exception as e:
            messages.info(request, str(e))
        response['error'] = False
        form = FormMakan(request.POST or None)
        response['form'] = form
    else:
        return login(request)
    return render(request, 'buat_makan.html', response)


def buat_rambut(request):
    cursor = connection.cursor()

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        cursor.execute("SET SEARCH_PATH TO the_cims")
        cursor.execute("select id_koleksi from rambut")
        id = namedtuplefetchall(cursor)
        for i in id:
            real_id = i
        id_bilangan = int(real_id[0][2:5]) + 1
        id_bil_converted = str(id_bilangan)
        form = FormBuatRambut(request.POST or None)
    if (request.method == "POST"):
        print('a')
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            harga_jual = form.cleaned_data.get('harga_jual')
            tipe = form.cleaned_data['tipe']
            print('c')
            new_id = 'RB'+id_bil_converted.zfill(3)
            print(new_id)
            cursor.execute('insert into koleksi (id, harga) values(%s, %s);',[new_id, harga_jual])
            cursor.execute('insert into rambut (id_koleksi, tipe) values(%s,%s);',[new_id, tipe])
            return redirect('/read/list_koleksi')
    print('b')
    return render(request, 'buat_rambut.html', {'id':id_bil_converted.zfill(3)})

@csrf_exempt
def buat_mata(request):
    cursor = connection.cursor()

    if "isLogin" not in request.session:
            return login(request)

    if request.session['role'] == 'Admin':
        cursor.execute("SET SEARCH_PATH TO the_cims")
        cursor.execute('select id_koleksi from mata')
        id = namedtuplefetchall(cursor)
        for i in id:
            real_id = i
        id_bilangan = int(real_id[0][2:5]) + 1
        id_bil_converted = str(id_bilangan)
        form = FormBuatMata(request.POST or None)
    if (request.method == "POST"):
        print('a')
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            harga_jual = form.cleaned_data.get('harga_jual')
            warna = form.cleaned_data['warna']
            print('c')
            new_id = 'MT'+id_bil_converted.zfill(3)
            print(new_id)
            cursor.execute('insert into koleksi (id, harga) values(%s, %s);',[new_id, harga_jual])
            cursor.execute('insert into mata (id_koleksi, warna) values(%s,%s);',[new_id, warna])
            return redirect('/read/list_koleksi')
    print('b')
    return render(request, 'buat_mata.html', {'id':id_bil_converted.zfill(3)})

def buat_rumah(request):
    cursor = connection.cursor()

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        cursor.execute("SET SEARCH_PATH TO the_cims")
        cursor.execute("select max(id_koleksi) from rumah")
        id_ = namedtuplefetchall(cursor)
        for i in id_:
            real_id = i
        id_bilangan = int(real_id[0][2:5]) + 1
        id_bil_converted = str(id_bilangan)
        form = FormBuatRumah(request.POST or None)
    if (request.method == "POST"):
        print('a')
        print(form.is_valid())
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            nama = form.cleaned_data['nama']
            harga_jual = form.cleaned_data.get('harga_jual')
            harga_beli = form.cleaned_data['harga_beli']
            kapasitas_barang = form.cleaned_data['kapasitas_barang']
            print('c')
            new_id = 'RM'+id_bil_converted.zfill(3)
            print(new_id)
            cursor.execute('insert into koleksi (id, harga) values(%s, %s);',[new_id, harga_jual])
            cursor.execute('insert into koleksi_jual_beli (id_koleksi, harga_beli, nama) values(%s, %s, %s);',[new_id, harga_beli, nama])
            cursor.execute('insert into rumah (id_koleksi, kapasitas_barang) values(%s,%s);',[new_id, kapasitas_barang])
            return redirect('/read/list_koleksi')
    return render(request, 'buat_rumah.html', {'id':id_bil_converted.zfill(3)})

def buat_barang(request):
    cursor = connection.cursor()

    if "isLogin" not in request.session:
        return login(request)

    if request.session['role'] == 'Admin':
        cursor.execute("SET SEARCH_PATH TO the_cims")
        cursor.execute("select max(id_koleksi) from barang")
        id_ = namedtuplefetchall(cursor)
        for i in id_:
            real_id = i
        id_bilangan = int(real_id[0][2:5]) + 1
        id_bil_converted = str(id_bilangan)
        form = FormBuatBarang(request.POST or None)
    if (request.method == "POST"):
        print('a')
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            nama = form.cleaned_data['nama']
            harga_jual = form.cleaned_data['harga_jual']
            harga_beli = form.cleaned_data['harga_beli']
            tingkat_energi = form.cleaned_data['tingkat_energi']
            print('c')
            new_id = 'BR'+id_bil_converted.zfill(3)
            print(new_id)
            cursor.execute('insert into koleksi (id, harga) values(%s, %s);',[new_id, harga_jual])
            cursor.execute('insert into koleksi_jual_beli (id_koleksi, harga_beli, nama) values(%s, %s, %s);',[new_id, harga_beli, nama])
            cursor.execute('insert into barang (id_koleksi, tingkat_energi) values(%s,%s);',[new_id, tingkat_energi])
            return redirect('/read/list_koleksi')
    return render(request, 'buat_barang.html', {'id':id_bil_converted.zfill(3)})

def buat_apparel(request):
    cursor = connection.cursor()
    if "isLogin" not in request.session:
            return login(request)

    if request.session['role'] == 'Admin':
        cursor.execute("SET SEARCH_PATH TO the_cims")
        cursor.execute("select max(id_koleksi) from apparel")
        id_ = namedtuplefetchall(cursor)
        for i in id_:
            real_id = i
        id_bilangan = int(real_id[0][2:5]) + 1
        id_bil_converted = str(id_bilangan)
        form = FormBuatApparel(request.POST or None)
        cursor.execute('select * from kategori_apparel')
        kategori = namedtuplefetchall(cursor)
        cursor.execute('select * from pekerjaan')
        nama_pekerjaan = namedtuplefetchall(cursor)

    if (request.method == "POST"):
        print('a')
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            nama = form.cleaned_data['nama']
            harga_jual = form.cleaned_data['harga_jual']
            harga_beli = form.cleaned_data['harga_beli']
            warna_apparel = form.cleaned_data['warna']
            kategori_apparel = request.POST.get('kategori_apparel')
            np = request.POST.get('nama_pekerjaan')
            print('c')
            new_id = 'AP'+id_bil_converted.zfill(3)
            print(new_id)
            cursor.execute('insert into koleksi (id, harga) values(%s, %s);',[new_id, harga_jual])
            cursor.execute('insert into koleksi_jual_beli (id_koleksi, harga_beli, nama) values(%s, %s, %s);',[new_id, harga_beli, nama])
            cursor.execute('insert into apparel (id_koleksi,nama_pekerjaan, kategori_apparel, warna_apparel) values(%s,%s,%s,%s);',[new_id, np,kategori_apparel, warna_apparel])
            return redirect('/read/list_koleksi')
    return render(request, 'buat_apparel.html', {'id':id_bil_converted.zfill(3), 'kategori_apparel':kategori, 'nama_pekerjaan':nama_pekerjaan})

def delete_kategori_apparel(request, nama):
    cursor = connection.cursor()
    cursor.execute('set search_path to thecims')
    cursor.execute('delete from kategori_apparel where nama_kategori = %s', [nama])
    return redirect('/kategori_apparel')

def buat_koleksi_tokoh(request):
    uname = request.session['username']
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO the_cims")
    cursor.execute(f"SELECT * FROM tokoh WHERE username_pengguna = '{uname}'")
    tokoh = namedtuplefetchall(cursor)
    print(tokoh)
    cursor.execute('select * from koleksi')
    koleksi = namedtuplefetchall(cursor)
    form = CreateKoleksiTokoh(request.POST or None)
    if (request.method == "POST"):
        print('a')
        if (form.is_valid()):
            # new_id = form.cleaned_data['new_id']
            nama_tokoh = form.cleaned_data.get('nama')
            id_koleksi = form.cleaned_data['koleksi']
            print(id_koleksi)
            cursor.execute('insert into koleksi_tokoh (id_koleksi, username_pengguna, nama_tokoh) values(%s, %s, %s);',[id_koleksi, uname, nama_tokoh])
            return redirect('/read/list_koleksi_tokoh')
    return render (request, 'buat_koleksi_tokoh.html', {'tokoh':tokoh, 'koleksi':koleksi})
