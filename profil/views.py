from django.http import response
from django.shortcuts import render,redirect
from django.http.response import HttpResponse, HttpResponseRedirect
from django.db import connection

# Create your views here.
def login_required(function):
    def wrapper(request, *args, **kwargs):
        if 'isLogin' not in request.session:
            return HttpResponseRedirect('/login/')
        else:
            return function(request, *args, **kwargs)
    return wrapper

# @login_required
# def profil(request):
#     # context = {}
#     if request.session['role'] == 'penggalang_dana_individu':
#         return HttpResponseRedirect('profile_penggalang')
#     elif request.session['role'] == 'penggalang_dana_organisasi':
#         return HttpResponseRedirect('profile_penggalang_2')
    
#     return render(request, 'profile_admin.html')



@login_required
def detail(request):
    
    if request.method == 'GET':
        if request.GET.get('email') is not None:
            email = request.GET.get('email')
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi, p.nomor_hp, p.alamat, p.nama_bank,  p.norek, i.NIK, i.tanggal_lahir, i.jenis_kelamin, p.link_repo, p.saldo_dona_pay  FROM SIDONA.PENGGALANG_DANA as p, 
                SIDONA.INDIVIDU as i WHERE i.email = '{email}' AND i.email = p.email LIMIT 1;""")
                individu = fetch(cursor)
                
                cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi, p.nomor_hp, p.alamat, p.norek, p.nama_bank, o.nama_organisasi, o.nap, o.no_telp_organisasi, o.tahun_berdiri, p.link_repo, p.saldo_dona_pay FROM SIDONA.PENGGALANG_DANA as p, 
                SIDONA.ORGANISASI as o WHERE o.email = '{email}' AND o.email = p.email LIMIT 1;""")
                organisasi = fetch(cursor)
                
                if individu:
                    return render(request, 'detail.html', {'individu' : individu[0]})
                else:
                    return render(request, 'detail.html', {'organisasi' : organisasi[0]})
    return render(request, 'profile_admin.html')
    # return render(request, 'detail.html', {'d':data[0]})
    
# @login_required
# def detail_pasien(request):
#     print(request.session['role'])
#     if request.session['role'] != 'Pengguna':
#         return redirect('login:landing')
#     if request.method == 'GET':
#         if request.GET.get('nik') is not None:
#             nik = request.GET.get('nik')
#             with connection.cursor() as cursor:
#                 cursor.execute(f"SELECT * FROM SIRUCO.PASIEN where NIK='{nik}' LIMIT 1;")
#                 data = dictfetchall(cursor)
#     return render(request, 'pasien/detailpasien.html', {'d':data[0]})
def read_pengguna(request):
    response = {}
    if request.session['role'] == 'Admin':
        return HttpResponseRedirect('/home/')
    else:
        # try:
            email = request.session['email']
            role = request.session['role']
            with connection.cursor() as cursor:
                if role == 'Individu':
                    cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi, p.nomor_hp, p.alamat, p.nama_bank,  p.norek, i.NIK, i.tanggal_lahir, i.jenis_kelamin, p.link_repo, p.saldo_dona_pay  FROM SIDONA.PENGGALANG_DANA as p, 
                    SIDONA.INDIVIDU as i WHERE i.email = '{email}' AND i.email = p.email LIMIT 1;""")
                    individu = fetch(cursor)
                    response['individu'] = individu[0]
                    cursor.execute(f"""SELECT w.idpd, p.judul, k.nama_kategori FROM SIDONA.WISHLIST_DONASI as w, SIDONA.KATEGORI_PD as k, 
                    SIDONA.PENGGALANGAN_DANA_PD as p, SIDONA.INDIVIDU as i WHERE w.idpd = p.id AND w.email = i.email AND k.id = p.id_kategori AND w.email = '{email}';""")
                    wishlist = fetch(cursor)
                    cursor.execute(f"SELECT jumlah_wishlist FROM SIDONA.INDIVIDU WHERE email = '{email}';")
                    jml = cursor.fetchone()
                    response['wishlist'] = wishlist
                    response['jml'] = jml[0]
                else:
                    cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi, p.nomor_hp, p.alamat, p.norek, p.nama_bank, o.nama_organisasi, o.nap, o.no_telp_organisasi, o.tahun_berdiri, p.link_repo, p.saldo_dona_pay FROM SIDONA.PENGGALANG_DANA as p, 
                    SIDONA.ORGANISASI as o WHERE o.email = '{email}' AND o.email = p.email LIMIT 1;""")
                    organisasi = fetch(cursor)
                    response['organisasi'] = organisasi[0]
    
        # except Exception as e:
        #     return HttpResponseRedirect('/home/')
   
    return render(request, 'profile_pengguna.html', response)

def read_admin(request):
    response = {}
    if request.session['role'] != 'Admin':
        return HttpResponseRedirect('/home/')
    else:
        try:
            # admin = request.session['email']
            with connection.cursor() as cursor:
                cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi FROM SIDONA.PENGGALANG_DANA as p, 
                SIDONA.INDIVIDU as i WHERE i.email = p.email""")
                individu = fetch(cursor)
                cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi FROM SIDONA.PENGGALANG_DANA as p, 
                SIDONA.ORGANISASI as o WHERE o.email = p.email""")
                organisasi = fetch(cursor)
                response['organisasi'] = organisasi
                response['individu'] = individu
        except Exception as e:
            return HttpResponseRedirect('/home/')
   
    return render(request, 'profile_admin.html', response)

def verifikasi(request):
    response = {}
    if request.method == 'GET':
        if request.GET.get('email') is not None:
            email = request.GET.get('email')
            email_admin = request.session['email']
            
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"""UPDATE SIDONA.penggalang_dana set status_verifikasi = 'Terferifikasi', email_admin = '{email_admin}' where email = '{email}';""")
                return HttpResponseRedirect("profile_admin")
        except Exception as e:
            return HttpResponseRedirect('/home/')
   
    return render(request, 'profile_admin.html', response)

def delete_wishlist(request):
    response = {}
    if request.method == 'GET':
        if request.GET.get('email') is not None:
            email = request.GET.get('email')
            idpd = request.GET.get('idpd')
            try:
                with connection.cursor() as cursor:
                    cursor.execute(f"DELETE FROM SIDONA.WISHLIST_DONASI WHERE idpd = '{idpd}' AND email = '{email}';")
            #         cursor.execute(f"""SELECT p.email, p.nama, p.status_verifikasi, p.nomor_hp, p.alamat, p.nama_bank,  p.norek, i.NIK, i.tanggal_lahir, i.jenis_kelamin, p.link_repo, p.saldo_dona_pay  FROM SIDONA.PENGGALANG_DANA as p, 
            #         SIDONA.INDIVIDU as i WHERE i.email = '{email}' AND i.email = p.email LIMIT 1;""")
            #         individu = fetch(cursor)
            #         response['individu'] = individu[0]
            #         cursor.execute(f"""SELECT w.idpd, p.judul, k.nama_kategori FROM SIDONA.WISHLIST_DONASI as w, SIDONA.KATEGORI_PD as k, 
            #         SIDONA.PENGGALANGAN_DANA_PD as p, SIDONA.INDIVIDU as i WHERE w.idpd = p.id AND w.email = i.email AND k.id = p.id_kategori AND w.email = '{email}';""")
            #         wishlist = fetch(cursor)
            #         cursor.execute(f"SELECT jumlah_wishlist FROM SIDONA.INDIVIDU WHERE email = '{email}';")
            #         jml = cursor.fetchone()
            #         response['wishlist'] = wishlist
            #         response['jml'] = jml[0]
            except Exception as e:
                return HttpResponseRedirect('/home/')
    return redirect('/profil/profile_pengguna')
    
def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]