from django.urls import path
from . import views

urlpatterns = [
    path('profile_admin', views.read_admin, name='profile_admin'),
    path('profile_pengguna', views.read_pengguna, name='profile_pengguna'),
    path('detail', views.detail, name='detail'),
    path('verifikasi', views.verifikasi, name='verifikasi'),
    path('delete_wishlist/', views.delete_wishlist, name='delete_wishlist')
]